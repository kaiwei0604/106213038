<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	$sql = "SELECT `ordID`, `orderDate`, `status` FROM userOrder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "insert into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,1);";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function removeFromCart($serno ,$uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "delete from orderItem WHERE serno = ? and ordID = ? and quantity = 1";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $serno , $ordID ); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}


function checkout($uID, $address) {
	global $db;
	$ordID=_getCartID($uID);
	$sql = "update userorder set orderDate=now(),address=?,status=1 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address , $ordID ); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function addnewproduct($name, $price , $detail) {
	global $db;
	$sql = "INSERT INTO `product`( `name`, `price`, `detail`) VALUES (?,?,?)";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sis", $name, $price , $detail); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function productDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function deleteproduct($prdID) {
	global $db;
	$sql = "DELETE FROM `product` WHERE `prdID` = ?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
}

function customerList($i) {
	global $db;
	$sql = "SELECT `ordID`, `uID`,`orderDate`, `address` FROM userOrder WHERE status = ?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $i); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function deliver($ordID) {
	global $db;
	$sql = "UPDATE `userorder` SET `status`= '2' WHERE `ordID` = ?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
?>










